import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
import http from '@/common/js/http/'
import '@/utils/httpInterceptors.js'
App.mpType = 'app'

Vue.config.productionTip = false
Vue.use(uView);
//示例图片地址
Vue.prototype.$serverUrl = 'https://unidemo.dcloud.net.cn';
const app = new Vue({
    ...App
})
app.$mount()
