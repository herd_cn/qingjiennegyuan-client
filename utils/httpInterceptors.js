import http from '@/common/js/http/'
// 是否正在刷新的标记
let isRefreshing = false
// 重试队列，每一项将是一个待执行的函数形式
let requests = []
//设置baseUrl
// http.config.baseUrl = "http://localhost:8081/api/"
http.config.baseUrl = "http://h5api.jingliangerp.xyz/api/"
// 从localStorage中获取token
function getLocalToken () {
    const token = 	uni.getStorageSync("Token");
    return token
}

// 给实例添加一个setToken方法，用于登录后将最新token动态添加到header，同时将token保存在localStorage中
http.setToken = (token) => {
  http.defaults.headers['Authorization'] = token
	uni.setStorageSync("Token",token);
}

function refreshToken () {
    // http是当前已创建的axios实例
    return http.post('/refreshtoken').then(res => res.data)
}

//设置请求前拦截器
http.interceptor.request = (config) => {
    //添加通用参数
    config.header = {
		'content-type': 'application/json',
		'Authorization':getLocalToken()
    }
}
//设置请求结束后拦截器
http.interceptor.response=(response) => {
  // const { code } = response.statusCode
  console.log("++++++++++"+response.data.status);
  if (response.data.status == '2001') {
	  uni.showModal({
	  	title:'提示',
		content:response.data.message,
		showCancel:false,
		success: () => {
			uni.navigateTo({
				url:"/pages/login/login",
				animationDuration:1000
			})
		}
	  })
  //   const config = response.config
  //   if (!isRefreshing) {
  //     isRefreshing = true
  //     return refreshToken().then(res => {
  //       const { token } = res.data
  //       http.setToken(token)
  //       config.headers['Authorization'] = token
  //       config.baseURL = ''
  //       // 已经刷新了token，将所有队列中的请求进行重试
  //       requests.forEach(cb => cb(token))
  //       requests = []
  //       return http.request(config)
  //     }).catch(res => {
  //       console.error('refreshtoken error =>', res)
  //       window.location.href = '/'
  //     }).finally(() => {
  //       isRefreshing = false
  //     })
  //   } else {
  //     // 正在刷新token，将返回一个未执行resolve的promise
  //     return new Promise((resolve) => {
  //       // 将resolve放进队列，用一个函数形式来保存，等token刷新后直接执行
  //       requests.push((token) => {
  //         config.baseURL = ''
  //         config.headers['Authorization'] = token
  //         resolve(http.request(config))
  //       })
  //     })
  //   }
  }
  return response
}, error => {
  return Promise.reject(error)
}
