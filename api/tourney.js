import http from '@/common/js/http/'
//初始化
export const getTourneyInit = () => {
	return http.get(
		'tourney/getTourneyInit',
		{
		}
	)
}
//参与
export const createTourney = (data) => {
	return http.post(
		'tourney/createTourney',
	     data
	)
}
//投票
export const vote = (data) => {
	return http.post(
	'tourney/vote',
	 data
	)
}
//列表
export const getTourneyList = (id) => {
	return http.post(
		'tourney/list',
		{
		competitionId:id
		}
	)
}