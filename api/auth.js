import http from '@/common/js/http/'
//注册

export const register = (phone,password,code,invitation,username) => {
	return http.post(
	    'auth/register',
		{
		  phone: phone,
		  password:password,
		  code: code,
		  invitation:invitation,
		  username:username
		}
	)
}
export const passwordBack = (phone,password,code) => {
	return  http.post(
	'auth/passwordBack',
	 {
		  phone: phone,
		  password:password,
		  code: code
		}
	)
}
//登录

export const login = (phone,password) => {
	return  http.post(
		'auth/login',
	    {
		  phone: phone,
		  password:password
		}
	)
}
//退出
export const logout = () => {
	return  http.post(
		'auth/logout'
	)
}
//验证码
export const validateCode  = (phone,type) => {
	return  http.post(
	'sms/sendCode',
	{
		  phone: phone,
		  type : type
		}
	)
}