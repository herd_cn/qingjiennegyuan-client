import http from '@/common/js/http/'
//获得用户信息
export const getUserInfo = () => {
	return http.get(
	 'user/getUserInfo',
	 {
		}
	)
}
//修改用户信息
export const editUserInfo = () => {
	return http.post(
		'user/editUserInfo',
	 {
		}
	)
}
//创建挂卖
export const createUserSell = (from) => {
	return http.post(
		'userSell/createUserSell',
	 from
	)
}
//创建购买
export const createUserBuy = (form) => {
	return http.post(
		'userBuy/createUserBuy',
	 form
	)
}
//创建银行卡信息
export const createUserBank = (form) => {
	return http.post(
		'userBank/createUserBank',
	 form
	)
}
//创建提货信息
export const createUserTake = (form) => {
	return http.post(
		'userTake/createUserTake',
	 form
	)
}
//交易页面初始化
export const getUserSellList = () => {
	return http.post(
		'userSell/getUserSellList',
	 {
		}
	)
}
//上传凭证页面初始化
export const uploadCertInit = () => {
	return http.post(
		'userBuy/uploadCertInit',
	 {
		}
	)
}
//上传凭证
export const doUploadCert = (form) => {
	return http.post(
		'userBuy/doUploadCert',
	form
	)
}
//确认打款页面初始化
export const userAuditInit = () => {
	return http.post(
		'userBuy/userAuditInit',
	 {
		}
	)
}
//确认打款
export const doUserAudit = (form) => {
	return http.post(
		'userBuy/doUserAudit',
	 form
	)
}
//派货员页面初始化
export const deliveryInit = () => {
	return http.post(
		'userTake/deliveryInit',
	 {
		}
	)
}
//派货员修改实际到货
export const deliveryUpdate = (form) => {
	return http.post(
		'userTake/deliveryUpdate',
	form
	)
}
//收货页面初始化
export const receiveInit = () => {
	return http.post(
		'userTake/receiveInit',
	 {
		}
	)
}
//用户收货确认
export const userReceive = (form) => {
	return http.post(
		'userTake/userReceive',
	form
	)
}
//银行初始化
export const getUserBank = () => {
	return http.post(
		'userBank/getUserBank',
	 {
		}
	)
}
