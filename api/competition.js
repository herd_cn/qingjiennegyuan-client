import http from '@/common/js/http/'
//获得用户信息
export const getList = (data) => {
	return http.post(
		 'competition/list',
		 data
	)
}
//修改用户信息
export const createCompetition = (data) => {
	return http(
	    'competition/createCompetition',
		 data
	)
}